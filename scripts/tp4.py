import os
import csv
import argparse

class PageMaker:
html = "<!DOCTYPE html>
                    <html lang="en">
                    <head>
                    <meta charset="UTF-8">
                    <meta http-equiv="X-UA-Compatible" content="IE=edge">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>De Niro Movies - [Title]</title>
                    </head>
                    <body>
                    <h1>Film: [Title]</h1>
                    <p>Année de sortie: [Year]</p>
                    <p>Score obtenue: [Score]</p>
                    
                    </body>
                    </html>"

year = []
score = []
title = []

with open("deniro.csv", "r") as f:

    var1 = csv.reader(f, delimiter=",")
    next(var1)
    count = 0

    for l in var1:
        if count != 0 and count < 89:
            year.append(l[1])
            score.append(l[2])
            title.append(l[3])
            count += 1

            
pm = PageMarker("deniro.csv")
pm.generate_html("/tmp")

