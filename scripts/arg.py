import sys
#Affichage de tous les arguments  (type liste) fournis en ligne de commande 
print(sys.argv)
#affichage du premier arguiment en ligne de commande
print(sys.argv[1])

#affichage du premier arguiment en ligne de commande et le multiplier par lui meme il faut donner un int sinon erreur
arg1 = int(sys.argv[1])
print(arg1 * arg1)
