import sys
n = int(sys.argv[1])

if n < 0 or n > 1000:
    print("erreur votre chiffre doit etre entre 0 et 1000")
    exit(1)

print("tapez un entier je calcule la multiplication")

x = int(input())

result = x*n

for x in range(11):
    print("voici le resultat",x,"x",n,"=", result)

