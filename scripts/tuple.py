t = ()
print(type(t)) #tuple

coordsGps = (45.9632, 21.7891)
print(coordsGps)
print("latitude",coordsGps[0])
print("Longitude",coordsGps[1])


#interdit sur le tuple (lecture seule)
#coordsGps[0] = 46.3322

#ajout a la list existante
#l = [123, 125 ,146]
#l.append(144444)
#print(l)

#impossible pour les tuples (lecture seule)
#coordsGps.append(89.2658)

lat, lng = coordsGps #tuple unpacked
print(lat, lng)

