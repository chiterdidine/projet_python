#variables primitive en python

temperature  = 2
pi = 3.14
isEarthRound = True
training = 'Initiation au langage Python'

#affichage de type de variables 

print(type(temperature)) #int
print(type(pi)) #float
print(type(isEarthRound)) #bool
print(type(training)) #str
'''
print(2+2)
print(temperature+temperature)
print(training)
'''
#operation sur les variables

print(temperature + 5) #addition
print(temperature + "5") #concatenantion entre deux chaines
print(pi + 5) #addition entre un float et un 
print(isEarthRound + 10) #conversion implicite, True ==> 1
print("Le double de pi est: " +str(pi * 2)) #la fonction de conversion  str()

doublePi = pi * 2 #stockage en variable du retour d'une expr. arithmitique 
print(type(doublePi)) #float
doublePiStr = str(doublePi))

