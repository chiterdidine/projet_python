#afficher 5 fois bonjour
'''
print("Bonjour")
print("Bonjour")
print("Bonjour")
print("Bonjour")
print("Bonjour")
'''
#solution 2, la boucle while
count = 1
while count < 5:
    print("Bonjour")
    count += 1 #incrementation de 1 augment de 1 a chque fois qu'en rentre dans la boucle 
    
count2 = 5
while count2 > 0:
    print("Bey", count2)
    count2 -= 1 #décrementation de 1 (a chaque fois qu'on rentre dans la boucle count2 qui est egale a 5 deminue de 1 a chaque fois


#solution 3, boucle for ...in
for n in range(5):
    print("coucou",n)

    #sortie de boucle prématurée

    if n == 2:
        break



